package com.example.weathermvi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.weathermvi.WeatherFragment.Companion.STATE

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(
                    android.R.id.content,
                    WeatherFragment.newInstance(
                        savedInstanceState?.getParcelable(STATE) ?: WeatherMachine.WeatherState(
                            forecast = "TEST"
                        )
                    )
                )
                .commit()
        }
    }
}
