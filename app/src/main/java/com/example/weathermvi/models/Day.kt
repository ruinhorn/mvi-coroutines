package com.example.weathermvi.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Day(
    @SerialName("avgtemp_c")
    val avgtemp_c: Float,
    @SerialName("condition")
    val condition: Condition
)