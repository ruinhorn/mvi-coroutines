package com.example.weathermvi.models

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class WeatherUiModel(
    val celsius: String
)