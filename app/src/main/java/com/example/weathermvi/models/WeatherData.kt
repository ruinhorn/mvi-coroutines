package com.example.weathermvi.models

import com.example.weathermvi.models.Forecast
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class WeatherData(
    @SerialName("forecast")
    val forecast: Forecast
){
}