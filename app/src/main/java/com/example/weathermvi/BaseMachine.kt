package com.example.weathermvi

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

abstract class BaseMachine<E : BaseMachine.Event, U : BaseMachine.Update, S : BaseMachine.State>(
    initialState: S
) : ViewModel(), CoroutineScope {

    interface Event
    interface Update
    interface State

    override val coroutineContext: CoroutineContext = SupervisorJob() + Dispatchers.IO

    private val eventsChannel = Channel<E>(Channel.CONFLATED)
    private val updateChannel = Channel<Flow<U>>(Channel.UNLIMITED)
    private val stateChannel = ConflatedBroadcastChannel(initialState)

    val eventEmitter: SendChannel<E> = eventsChannel
    val stateSubscription: ReceiveChannel<S> = stateChannel.openSubscription()
    val state get() = stateChannel.value

    init {
        launch {
            eventsChannel.consumeEach { event ->
                updateChannel.send(processEvent(event))
            }
        }
        launch {
            updateChannel.consumeEach { updates ->
                updates.collect { update ->
                    stateChannel.send(updateState(update))
                }
            }
        }
    }

    protected abstract fun processEvent(event: E): Flow<U>

    protected abstract fun updateState(update: U): S

    override fun onCleared() {
        super.onCleared()
        coroutineContext.cancel()
    }
}