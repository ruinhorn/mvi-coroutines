package com.example.weathermvi

import android.app.Application
import com.example.weathermvi.di.module
import org.koin.core.context.startKoin
import org.koin.core.module.Module

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(module)
        }
    }
}