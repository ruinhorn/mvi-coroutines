package com.example.weathermvi.di

import com.example.weathermvi.WeatherMachine
import com.example.weathermvi.data.WeatherDataSource
import com.example.weathermvi.data.WeatherRepo
import com.example.weathermvi.data.WeatherRepoImpl
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import kotlinx.serialization.json.Json
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val module = module {
    single {
        HttpClient(OkHttp) {
            install(JsonFeature) {
                serializer = KotlinxSerializer(Json.nonstrict)
            }
        }
    }

    single { WeatherDataSource(get()) }
    single { WeatherRepoImpl(get()) as WeatherRepo }
    viewModel { WeatherMachine(get(), WeatherMachine.WeatherState(forecast = "FROM KOIN")) }
}