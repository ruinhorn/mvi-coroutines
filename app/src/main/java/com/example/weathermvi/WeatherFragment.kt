package com.example.weathermvi

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fmt_weather.currentWeather
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import kotlin.coroutines.CoroutineContext

class WeatherFragment : Fragment(), CoroutineScope {

    companion object {
        const val STATE = "STATE"
        fun newInstance(state: WeatherMachine.WeatherState?) = WeatherFragment().apply {
            arguments = Bundle().apply { putParcelable(STATE, state) }
        }
    }

    private val machine: WeatherMachine by viewModel()

    override val coroutineContext: CoroutineContext = SupervisorJob() + Dispatchers.Main

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fmt_weather, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        render(
            savedInstanceState?.getParcelable(STATE)
                ?: arguments?.getParcelable<WeatherMachine.WeatherState>(STATE)
                ?: WeatherMachine.WeatherState(forecast = "EMPTY")
        )
        launch {
            machine.stateSubscription.consumeEach(::render)
        }
        currentWeather.setOnClickListener {
            machine.eventEmitter.offer(WeatherMachine.Event.SearchSubmitted("test"))
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(STATE, machine.state)
        super.onSaveInstanceState(outState)
    }

    private fun render(state: WeatherMachine.WeatherState) {
        currentWeather.text = state.forecast
    }
}
