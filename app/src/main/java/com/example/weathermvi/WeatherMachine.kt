package com.example.weathermvi

import android.os.Parcelable
import com.example.weathermvi.data.WeatherRepo
import com.example.weathermvi.models.WeatherData
import kotlinx.android.parcel.Parcelize
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.lang.Exception

class WeatherMachine(private val repo: WeatherRepo, weatherState: WeatherState) :
    BaseMachine<WeatherMachine.Event, WeatherMachine.Update, WeatherMachine.WeatherState>(
        weatherState
    ), CoroutineScope {

    override fun updateState(update: Update) = when (update) {
        is Update.Progress -> state.copy(
            isInProgress = update.isInProgress
        )
        is Update.Results -> state.copy(
            query = update.query,
            forecast = update.searchResults.forecast.forecastday[0].day.avgtemp_c.toString()
        )
        is Update.Error -> state.copy(
            query = update.query,
            errorMessage = update.errorMessage,
            shouldShowError = true
        )
        Update.HideError -> state.copy(
            shouldShowError = false
        )
    }

    override fun processEvent(event: Event): Flow<Update> = when (event) {
        is Event.SearchSubmitted -> requestWeather(event.query)
        Event.RefreshClicked -> requestWeather(state.query)
        Event.ErrorMessageDismissed -> flow {
            emit(Update.HideError)
        }
    }

    fun requestWeather(query: String) = flow {
        emit(Update.Progress(true))
        try {
            val searchResult = repo.getWeather(query)
            emit(Update.Results(query, searchResult))
        } catch (e: Exception) {
            emit(Update.Error(query, e.toString()))
        } finally {
            emit(Update.Progress(false))
        }
    }

    @Parcelize
    data class WeatherState(
        val query: String = "",
        val forecast: String = "",
        val isInProgress: Boolean = false,
        val errorMessage: String = "",
        val shouldShowError: Boolean = false
    ) : Parcelable, State

    sealed class Event : BaseMachine.Event {
        data class SearchSubmitted(val query: String) : Event()
        object RefreshClicked : Event()
        object ErrorMessageDismissed : Event()
    }

    sealed class Update : BaseMachine.Update {
        data class Progress(val isInProgress: Boolean) : Update()
        data class Results(val query: String, val searchResults: WeatherData) : Update()
        data class Error(val query: String, val errorMessage: String) : Update()
        object HideError : Update()
    }

    override fun onCleared() {
        super.onCleared()
        coroutineContext.cancel()
    }
}
