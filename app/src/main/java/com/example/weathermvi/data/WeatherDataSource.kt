package com.example.weathermvi.data

import com.example.weathermvi.models.WeatherData
import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.okhttp.OkHttpConfig
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.http.URLProtocol

class WeatherDataSource(private val client: HttpClient) {

    suspend fun getForecast(): WeatherData = client.get {
        url {
            protocol = URLProtocol.HTTPS
            host = "api.apixu.com/v1/forecast.json"
//            encodedPath = "/v1/forecast.json"
            parameter("key", "cfb762863162437d82d95301181806")
            parameter("q", "55.45,37.37")
            parameter("days", "1")
        }
    }
}