package com.example.weathermvi.data

import com.example.weathermvi.models.WeatherData
import com.example.weathermvi.models.WeatherUiModel

class WeatherRepoImpl(private val weatherDataSource: WeatherDataSource): WeatherRepo {

    override suspend fun getWeather(query: String) =  weatherDataSource.getForecast()

    fun WeatherData.toForecast() = forecast.forecastday[0].day.avgtemp_c

}