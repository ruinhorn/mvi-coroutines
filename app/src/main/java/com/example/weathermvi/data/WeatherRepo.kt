package com.example.weathermvi.data

import com.example.weathermvi.models.WeatherData

interface WeatherRepo {

    suspend fun getWeather(query: String): WeatherData
}